package it.tiwiz.aw.speech.recognition;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class SpeechRecognitionActivity extends Activity
{
    /**
     * Called when the activity is first created.
     */

    private ListView listaRisultati;
    private boolean servizioRiconoscimentoPresente = true;

    private static final int CODICE_DI_RICHIESTA = 16; //questo codice ci serve solo per
    // essere sicuri di come processare l'input nel caso avviassimo più Activity - nel nostro caso è inutile,
    // ma è una Best Practice

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        listaRisultati = (ListView)findViewById(R.id.resultList);

        //controlliamo che sia disponibile il servizio per il riconoscimento vocale
        PackageManager pm = getPackageManager();
        Intent intentRiconoscimentoVocale = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        List<ResolveInfo> activities = pm.queryIntentActivities(intentRiconoscimentoVocale,0);
        if(activities.size() == 0){
            //non abbiamo il servizio, impostiamo il flag che evita di far partire l'intent
            servizioRiconoscimentoPresente = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId()){
        case R.id.abBtnSpeak:
            if(servizioRiconoscimentoPresente) avviaRiconoscimento();
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void avviaRiconoscimento(){
        //creiamo l'intent
        Intent intentRiconoscimentoVocale = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        //ne impostiamo la lingua di riconoscimento - RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        intentRiconoscimentoVocale.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        //impostiamo il limite massimo di risultati che ci aspettiamo
        intentRiconoscimentoVocale.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 10);
        //e indichiamo il titolo che vogliamo mostrare
        intentRiconoscimentoVocale.putExtra(RecognizerIntent.EXTRA_PROMPT, "Dimmi qualcosa!");
        startActivityForResult(intentRiconoscimentoVocale, CODICE_DI_RICHIESTA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        //controlliamo che il risultato sia "OK" e che il codice della richiesta sia quello che abbiamo specificato
        if (requestCode == CODICE_DI_RICHIESTA && resultCode == RESULT_OK)
        {
            //Inseriamo nella nostra lista i risultati che ha trovato il riconoscitore
            ArrayList<String> risultati = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            listaRisultati.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,risultati));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
